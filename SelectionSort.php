<?php
//not allow directly access
defined('INTERNAL') || die('No direct access allowed.');

require_once('model.class.php');

class SelectionSort implements sort_methods {

	public function sort_array($array) {
		$n = sizeof($array);
		for ($i = 0; $i < $n; $i++) {
			$lowestValueIndex = $i;
			$lowestValue = $array[$i];
			for ($j = $i + 1; $j < $n; $j++) {
				if ($array[$j] < $lowestValue) {
					$lowestValueIndex = $j;
					$lowestValue = $array[$j];
				}
			}
			$array[$lowestValueIndex] = $array[$i];
			$array[$i] = $lowestValue;
		}
		return $array;
	}
	
	public function getHtml($html, $sortMethod, $arr, $i, $description) {
		$html->output_sort($sortMethod, $arr, $i, $description);
	}
}
?>