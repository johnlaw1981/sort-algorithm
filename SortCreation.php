<?php
//not allow directly access
defined('INTERNAL') || die('No direct access allowed.');

require_once ('BubbleSort.php');
require_once ('QuickSort.php');
require_once ('SelectionSort.php');

class SortCreation{
	public static function createSort($type){
		if (strcasecmp($type,'Bubble Sort')==0){
			return new BubbleSort();
		} elseif (strcasecmp($type,'Quick Sort')==0){
				return new QuickSort();
		} elseif (strcasecmp($type,'Selection Sort')==0){
				return new SelectionSort();
		} else {
			return Null;
		}
	}
}

?>