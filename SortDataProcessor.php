<?php
//not allow directly access
defined('INTERNAL') || die('No direct access allowed.');

require_once ('SortCreation.php');

class SortDataProcessor {

	private $sortType = NULL; 
	
	private $type = NULL;
	
	private $sortedArray =NULL;
    
    public function __construct($type) {
    	
        $this->sortType = SortCreation::createSort($type);
		$this->type = $type;     		
    }
     
    public function sort_array($array) {
    	
    	if($this->sortType){
			$this->sortedArray = $this->sortType->sort_array($array);
    		return $this->sortedArray;
    	} else {
    		return NULL;
    	}
    }
	
	public function getHtml($html, $string) {
		$sortDescr=lcfirst(preg_replace('/\s+/', '', $this->type));
    	if($this->sortType){
    		return $this->sortType->getHtml($html, $this->type, $this->sortedArray, $sortDescr, $string[$sortDescr]);
    	} else {
    		return NULL;
    	}
    }
}
?>