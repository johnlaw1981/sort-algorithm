<?php
//****************************//
//Name: John Luo              //
//****************************//

//This file is part of test
define('INTERNAL',           1);
require_once('viewer.class.php');
require_once('lang.php');
require_once('config.php');
require_once ('SortDataProcessor.php');

$html = new viewer_framework();
$inputErr = "";

if($_SERVER["REQUEST_METHOD"] != "POST") $html->input_array($inputErr);
else {
	if ($inputErr = validation($_POST["array"])) {
		$html->input_array($inputErr);
	}
	else {
		$data = trim($_POST["array"]);
		$array = explode(",",$data);
		
		$html->panel();
		for ($i=0;$i<count($SA->type);$i++){
		
			$algo = new SortDataProcessor($SA->type[$i]);
			
			$sortedArray = $algo->sort_array($array);
			
			if($sortedArray){
				$algo->getHtml($html, $string);
			} else {
				echo "No result";
			}
		}
		$html->closepanel();
	}
}

$html->printhtml();

function validation ($data) {
	if ($data) {
		$data = stripslashes($data);
		$data = htmlspecialchars($data);
		if (!preg_match("/^[0-9, ]+$/",$data)) {
			return "Only number and comma allowed";
		}
		else return "";
	}
	else return "Input is required";
}
?>



