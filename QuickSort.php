<?php
//not allow directly access
defined('INTERNAL') || die('No direct access allowed.');

require_once('model.class.php');

class QuickSort implements sort_methods {

	public function sort_array($array) {
		$length = count($array);

		if ($length <= 1)
			return $array;

		$left = $right = array();
		$mid_index = $length>>1;
		$mid_value = $array[$mid_index];

		for ($i = 0; $i < $length; $i++) {
			if ($i == $mid_index)
				continue;

			if ($array[$i] < $mid_value)
				$left[] = $array[$i];
			else
				$right[] = $array[$i];
		}
		return array_merge($this->sort_array($left), (array)$mid_value, $this->sort_array($right));
	}
	
	public function getHtml($html, $sortMethod, $arr, $i, $description) {
		$html->output_sort($sortMethod, $arr, $i, $description);
	}
}
?>