<?php
//not allow directly access
defined('INTERNAL') || die('No direct access allowed.');

interface Sort_Methods {
	public function sort_array($arr);
    public function getHtml($html, $sortMethod, $arr, $i, $description);
}
