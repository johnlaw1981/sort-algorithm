<?php
//not allow directly access
defined('INTERNAL') || die('No direct access allowed.');

class viewer_framework {
	/**
     * @var string import fileanme
     */
	private $html;
	
	public function __construct() {
        $this->html  = "<!--HTML code--> \n";
		$this->html  .= "<!DOCTYPE html> \n";
		$this->html  .= "<html> \n";
		$this->html  .= "<head> \n";
		$this->html  .= "<title>sort</title> \n";
		$this->html  .= "<meta charset=\"utf-8\"> \n";
		$this->html  .= "<meta name=\"viewport\" content=\"width=device-width, initial-scale=1\"> \n";
		$this->html  .= "<link rel=\"stylesheet\" href=\"./bootstrap/css/bootstrap.min.css\"> \n";
		$this->html  .= "<link rel='stylesheet' href='main.css'> \n";
		$this->html  .= "<script src=\"./bootstrap/js/jquery.min.js\"></script> \n";
		$this->html  .= "<script src=\"./bootstrap/js/bootstrap.min.js\"></script>  \n";
		$this->html  .= "</head> \n";
		$this->html  .= "<body> \n";
		$this->html  .= "<div class=\"container-fluid banner text-center\"> \n";
		$this->html  .= "<h3>Sorting algorithm</h3> \n";
		$this->html  .= "<img src=\"algorithm.png\" class=\"img-circle\" alt=\"Bird\"> \n";
		$this->html  .= "<div class=\"space\"> </div>\n";
		$this->html  .= "</div> \n";
		
    }
	
	public function input_array($inputErr) {
		$this->html  .= "<div class=\"container-fluid content text-center\"> \n";
		$this->html  .= "<form action=\"index.php\" method=\"post\"> \n";
		$this->html  .= "<div class=\"form-group\"> \n";
        $this->html  .= "<label for=\"message-text\" class=\"control-label\"><h3>Input arbitrary array</h3></label> \n";
		$this->html  .= "<p>example: 21, 34, 3, 32, 82, 55, 89, 50, 37, 5, 64, 35, 9, 70</p>";
        $this->html  .= "<textarea class=\"form-control\" id=\"message-text\" name=\"array\"></textarea> ";
		$this->html  .= "<span class=\"error\">".$inputErr."</span> \n";
        $this->html  .= "</div> \n";
		$this->html  .= "<input type=\"Submit\" name=\"submit\" value=\"Submit\" class=\"black\"> \n";
        $this->html  .= "</form> \n";
		$this->html  .= "<div class=\"space\"></div>\n";
		$this->html  .= "</div> \n";
	}
	
	Public function panel() {
		$this->html  .= "<div class=\"container\"> \n";
		$this->html  .= "<div class=\"panel-group\" id=\"accordion\"> \n";
	}
	
	public function output_sort($sortMethod, $arr, $i, $description) {
		$this->html  .= "<div class=\"panel panel-default\"> \n";
        $this->html  .= "<div class=\"panel-heading\"> \n";
        $this->html  .= "<h4 class=\"panel-title\"> \n";
        $this->html  .= "<a data-toggle=\"collapse\" data-parent=\"#accordion\" href=\"#collapse".$i."\">".$sortMethod."</a> \n";
        $this->html  .= "</h4> \n";
        $this->html  .= "</div> \n";
        $this->html  .= "<div id=\"collapse".$i."\" class=\"panel-collapse collapse\"> \n";
        $this->html  .= "<div class=\"panel-body\">".$description." \n";
		$this->html  .= "<h4>Result: </h4>\n";
		for ($i = 0; $i < count($arr); $i++) {
			$this->html  .= $arr[$i] . ' ';
		}
		$this->html  .= "</div> \n";
        $this->html  .= "</div> \n";
        $this->html  .= "</div> \n";
		
	}
	
	Public function closepanel() {
		$this->html  .= "<div class=\"space\"> </div>\n";
		$this->html  .= "<form action=\"index.php\"> \n";
		$this->html  .= "<input type=\"submit\" name=\"back\" value=\"Back\" class=\"black\"> \n";
		$this->html  .= "</form> \n";
		$this->html  .= "</div> \n";
		$this->html  .= "</div> \n";
	}
	
	public function printhtml (){
		echo $this->html;
	}
}