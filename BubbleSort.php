<?php
//not allow directly access
defined('INTERNAL') || die('No direct access allowed.');

require_once('model.class.php');

class BubbleSort implements Sort_Methods {

	public function sort_array($array) {
		$n = sizeof($array);
		for ($i = 1; $i < $n; $i++) {
			for ($j = $n - 1; $j >= $i; $j--) {
				if($array[$j-1] > $array[$j]) {
					$tmp = $array[$j - 1];
					$array[$j - 1] = $array[$j];
					$array[$j] = $tmp;
				}
			}
		}
		return $array;
	}
	
	public function getHtml($html, $sortMethod, $arr, $i, $description) {
		$html->output_sort($sortMethod, $arr, $i, $description);
	}
}
?>