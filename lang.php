<?php
$string["bubbleSort"] = "Bubble sort is a simple sorting algorithm that repeatedly steps through the list
						 to be sorted, compares each pair of adjacent items and swaps them if they are in 
						 the wrong order. The pass through the list is repeated until no swaps are needed,
						 which indicates that the list is sorted. The algorithm, which is a comparison sort, 
						 is named for the way smaller or larger elements 'bubble' to the top of the list. 
						 Although the algorithm is simple, it is too slow and impractical for most problems 
						 even when compared to insertion sort.";
$string["quickSort"] = "Quicksort is an efficient sorting algorithm, serving as a systematic method for 
						placing the elements of an array in order. Developed by Tony Hoare in 1959, with
						his work published in 1961, it is still a commonly used algorithm for sorting. 
						When implemented well, it can be about two or three times faster than its main competitors,
						merge sort and heapsort";
$string["selectionSort"] = "Selection sort is a sorting algorithm, specifically an in-place comparison sort. It has
							O(n2) time complexity, making it inefficient on large lists, and generally performs worse
							than the similar insertion sort. Selection sort is noted for its simplicity, and it has 
							performance advantages over more complicated algorithms in certain situations, particularly
							where auxiliary memory is limited.";